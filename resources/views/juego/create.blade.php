@extends('layouts.app')
@section('content')

    @if ($errors->any())

        <div class="alert alert-danger" role="alert">
            <p class="mb-0">{{ __('Errors') }}</p>
            <ul class="mb-0">

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>
        </div>

    @endif

    {!! Form::open(['route' => 'games.store', 'method' => 'post']) !!}

        <div class="form-group">

            {{ Form::label('first_player_name', __('Primer jugador')) }}

            {{ Form::text('first_player_name', old('Primer jugador'), ['class' => 'form-control'
                . ($errors->has('Primer jugador') ? ' is-invalid' : ''), 'placeholder' => __('Nombre')]) }}

            @if ($errors->has('first_player_name'))
                <div class="invalid-feedback">

                    {{ $errors->first('first_player_name') }}

                </div>
            @endif

        </div>

        {!! Form::submit(__('Iniciar'), ['class' => 'btn btn-primary']) !!}

    {!! Form::close() !!}

@endsection
